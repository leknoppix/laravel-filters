<?php declare(strict_types=1);

namespace Leknoppix\LaravelFilters\Middlewares;

use Leknoppix\LaravelFilters\Filter;

class SplitArguments
{
    public function __invoke(Filter $filter, $callback)
    {
        if (! $this->hasArguments($filter->callable)) {
            return $callback($filter);
        }

        list($callable, $arguments) = explode(':', $filter->callable);

        return $callback(new Filter($callable, explode(',', $arguments)));
    }

    protected function hasArguments($callable)
    {
        return is_string($callable) AND str_contains($callable, ':');
    }
}