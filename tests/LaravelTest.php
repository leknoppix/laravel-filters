<?php declare(strict_types=1);

namespace Leknoppix\LaravelFilters;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Orchestra\Testbench\TestCase;

class LaravelTest extends TestCase
{
    /** @test */
    function filter requests before validation()
    {
        $this->withoutExceptionHandling();

        try {
            $response = $this->post('/before', [
                'name' => 'JOHN',
            ]);

            $this->assertEquals('john', $response->getContent());
        } catch (ValidationException $e) {
            $this->fail("The `strtolower` filter didn't run for 'JOHN' before the validation rule (must be 'john').");
        }
    }

    /** @test */
    function filter requests after validation()
    {
        $this->withoutExceptionHandling();

        try {
            $response = $this->post('/after', [
                'password' => 'azerty22',
                'password_confirmation' => 'azerty22',
            ]);

            $this->assertTrue(Hash::check('azerty22', $response->getContent()));
        } catch (ValidationException $e) {
            $this->fail("The `bcrypt` filter ran for 'azerty22' before the validation rule (must be confirmed).");
        }
    }

    /** @test */
    function filter request in the controller()
    {
        $this->withoutExceptionHandling();

        $response = $this->post('/withoutFormRequest', [
            'name' => 'JOHN',
        ]);

        $this->assertEquals('john', $response->getContent());
    }

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class, TestServiceProvider::class];
    }
}

class RequestWithBeforeFilters extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => [Rule::in('john')],
        ];
    }

    public function filters()
    {
        return [
            'name' => ['strtolower'],
        ];
    }
}

class RequestWithAfterFilters extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password' => ['confirmed'],
        ];
    }

    public function afterFilters()
    {
        return [
            'password' => ['bcrypt'],
        ];
    }
}

class TestServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        Route::post('/before', function (RequestWithBeforeFilters $request) {
            return $request->name;
        });
        Route::post('/after', function (RequestWithAfterFilters $request) {
            return $request->password;
        });
        Route::post('/withoutFormRequest', function () {
            return request()
                ->filter([
                    'name' => ['strtolower'],
                ])
                ->name;
        });
    }
}
