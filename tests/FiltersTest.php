<?php declare(strict_types=1);

namespace Leknoppix\LaravelFilters;

use PHPUnit\Framework\TestCase;

class FiltersTest extends TestCase
{
    /** @test */
    function filters the data with basic PHP function()
    {
        $filters = new Filters([
            'name' => 'trim',
        ]);

        $data = $filters([
            'name' => ' Jane  ',
        ]);

        $this->assertEquals([
            'name' => 'Jane',
        ], $data);
    }

    /** @test */
    function filters the data with a null value should return unchanged()
    {
        $filters = new Filters([
            'name' => function ($value) {
                if (is_null($value)) {
                    $this->fail("The filter shouldn't be called with null values.");
                }
            },
        ]);

        $data = $filters([
            'name' => null,
        ]);

        $this->assertEquals([
            'name' => null,
        ], $data);
    }

    /** @test */
    function filters the data with closure()
    {
        $filters = new Filters([
            'name' => function ($value) {
                return trim($value);
            },
        ]);

        $data = $filters([
            'name' => ' Jane  ',
        ]);

        $this->assertEquals([
            'name' => 'Jane',
        ], $data);
    }

    /** @test */
    function filters the data with invokable object()
    {
        $filters = new Filters([
            'name' => new class {
                public function __invoke($value)
                {
                    return trim($value);
                }
            },
        ]);

        $data = $filters([
            'name' => ' Jane  ',
        ]);

        $this->assertEquals([
            'name' => 'Jane',
        ], $data);
    }

    /** @test */
    function can have multiple filters()
    {
        $filters = new Filters([
            'name' => ['trim', 'strtoupper'],
        ]);

        $data = $filters([
            'name' => ' Jane  ',
        ]);

        $this->assertEquals([
            'name' => 'JANE',
        ], $data);
    }

    /** @test */
    function it doesnt filter other data()
    {
        $filters = new Filters([
            'name' => 'trim',
        ]);

        $data = $filters([
            'name' => ' Jane  ',
            'password' => ' secret '
        ]);

        $this->assertEquals([
            'name' => 'Jane',
            'password' => ' secret ',
        ], $data);
    }

    /** @test */
    function it doesn t filter null data()
    {
        $filters = new Filters([
            'name' => 'trim',
        ]);

        $data = $filters([
            'name' => null,
        ]);

        $this->assertEquals([
            'name' => null,
        ], $data);
    }

    /** @test */
    function can resolve a filter from a container()
    {
        Filters::$container = new class {
            public function make($id)
            {
                return function ($value) {
                    return strtoupper($value);
                };
            }

            public function bound($id)
            {
                return $id === 'container-id';
            }
        };

        $filters = new Filters([
            'name' => ['trim', 'container-id'],
        ]);

        $data = $filters([
            'name' => ' Jane  ',
        ]);

        $this->assertEquals([
            'name' => 'JANE',
        ], $data);
    }

    /** @test */
    function can call functions with arguments()
    {
        $filters = new Filters([
            'name' => 'ucwords:$$,-',
        ]);

        $data = $filters([
            'name' => 'anne-marie',
        ]);

        $this->assertEquals([
            'name' => 'Anne-Marie',
        ], $data);
    }

    /** @test */
    function can call functions with arguments in any order()
    {
        $filters = new Filters([
            'name' => 'str_replace:-, ,$$',
        ]);

        $data = $filters([
            'name' => 'Anne-Marie',
        ]);

        $this->assertEquals([
            'name' => 'Anne Marie',
        ], $data);
    }

    /** @test */
    function can resolve a function with arguments()
    {
        Filters::$container = new class {
            public function make($id)
            {
                return function ($value, $delimiters) {
                    return ucwords($value, $delimiters);
                };
            }

            public function bound($id)
            {
                return $id === 'my-function';
            }
        };

        $filters = new Filters([
            'name' => 'my-function:$$,-',
        ]);

        $data = $filters([
            'name' => 'anne-marie',
        ]);

        $this->assertEquals([
            'name' => 'Anne-Marie',
        ], $data);
    }
}
